# Accessibility-demo

An application testing accessibility features on flutter. 
Features in the demo includes:
- Selection Font size (small, medium and large).
- Selection theme (Light/dark).
- Selection language (English/spanish).
- Go to accessibility device settings. 
- Compatibility with screenReader.


## Getting Started
- Download zip project or clone git repository on your local workspace.
- Connect mobile device to your computer, or start emulated device.
- Run "flutter doctor -v" to check everything is ok.
- Run "flutter run" or "flutter run -v" for more info.

Used Packages:
- flutter_localizations
- system_settings
- flutter_intl
- flutter_svg
- provider

## Screenshoots

<p align="center">
  <img src="assets/screenshoots/sc01.jpg" width="200" alt="1"/>
  <img src="assets/screenshoots/sc02.jpg" width="200" alt="2"/> 
  <img src="assets/screenshoots/sc03.jpg" width="200" alt="3"/>  
</p>


<p align="center">
  <img src="assets/screenshoots/sc04.jpg" width="200" alt="4"/>
  <img src="assets/screenshoots/demo.gif" width="200" alt="g1"/>
</p>

**-Screenshoot get on Android Lollipop**
# Authors
* **José Martínez** - [JMartinezCL](https://gitlab.com/JMartinezCL)

