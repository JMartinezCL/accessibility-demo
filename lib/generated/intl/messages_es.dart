// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a es locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'es';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "English" : MessageLookupByLibrary.simpleMessage("Ingles"),
    "FontSize" : MessageLookupByLibrary.simpleMessage("Tamaño de texto"),
    "GoSettings" : MessageLookupByLibrary.simpleMessage("Ir a ajustes"),
    "Language" : MessageLookupByLibrary.simpleMessage("Idioma"),
    "LanguageChanged" : MessageLookupByLibrary.simpleMessage("Idioma cambiado"),
    "ListLanguageOptions" : MessageLookupByLibrary.simpleMessage("Lista de idiomas, presiona para desplegar opciones"),
    "Menu" : MessageLookupByLibrary.simpleMessage("Menu de accesibilidad"),
    "SRStateOff" : MessageLookupByLibrary.simpleMessage("Lector de pantalla apagado"),
    "SRStateOn" : MessageLookupByLibrary.simpleMessage("Lector de pantalla encendido"),
    "ScreenReader" : MessageLookupByLibrary.simpleMessage("Lector de pantalla"),
    "SelectLanguage" : MessageLookupByLibrary.simpleMessage("Selecciona el idioma:"),
    "Spanish" : MessageLookupByLibrary.simpleMessage("Español"),
    "Theme" : MessageLookupByLibrary.simpleMessage("Tema"),
    "darkMode" : MessageLookupByLibrary.simpleMessage("Modo oscuro"),
    "large" : MessageLookupByLibrary.simpleMessage("Grande"),
    "lightMode" : MessageLookupByLibrary.simpleMessage("Modo claro"),
    "medium" : MessageLookupByLibrary.simpleMessage("Mediano"),
    "selectFont" : MessageLookupByLibrary.simpleMessage("Selecciona el tamaño de texto:"),
    "small" : MessageLookupByLibrary.simpleMessage("Pequeño")
  };
}
