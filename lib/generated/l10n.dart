// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values

class S {
  S();
  
  static S current;
  
  static const AppLocalizationDelegate delegate =
    AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false) ? locale.languageCode : locale.toString();
    final localeName = Intl.canonicalizedLocale(name); 
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      S.current = S();
      
      return S.current;
    });
  } 

  static S of(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `Accessibility Menu`
  String get Menu {
    return Intl.message(
      'Accessibility Menu',
      name: 'Menu',
      desc: '',
      args: [],
    );
  }

  /// `Small`
  String get small {
    return Intl.message(
      'Small',
      name: 'small',
      desc: '',
      args: [],
    );
  }

  /// `Medium`
  String get medium {
    return Intl.message(
      'Medium',
      name: 'medium',
      desc: '',
      args: [],
    );
  }

  /// `Large`
  String get large {
    return Intl.message(
      'Large',
      name: 'large',
      desc: '',
      args: [],
    );
  }

  /// `Dark mode`
  String get darkMode {
    return Intl.message(
      'Dark mode',
      name: 'darkMode',
      desc: '',
      args: [],
    );
  }

  /// `Light mode`
  String get lightMode {
    return Intl.message(
      'Light mode',
      name: 'lightMode',
      desc: '',
      args: [],
    );
  }

  /// `Select font size:`
  String get selectFont {
    return Intl.message(
      'Select font size:',
      name: 'selectFont',
      desc: '',
      args: [],
    );
  }

  /// `Screen reader is on`
  String get SRStateOn {
    return Intl.message(
      'Screen reader is on',
      name: 'SRStateOn',
      desc: '',
      args: [],
    );
  }

  /// `Screen reader is off`
  String get SRStateOff {
    return Intl.message(
      'Screen reader is off',
      name: 'SRStateOff',
      desc: '',
      args: [],
    );
  }

  /// `Select Language:`
  String get SelectLanguage {
    return Intl.message(
      'Select Language:',
      name: 'SelectLanguage',
      desc: '',
      args: [],
    );
  }

  /// `Language`
  String get Language {
    return Intl.message(
      'Language',
      name: 'Language',
      desc: '',
      args: [],
    );
  }

  /// `Theme`
  String get Theme {
    return Intl.message(
      'Theme',
      name: 'Theme',
      desc: '',
      args: [],
    );
  }

  /// `Font size`
  String get FontSize {
    return Intl.message(
      'Font size',
      name: 'FontSize',
      desc: '',
      args: [],
    );
  }

  /// `Screen Reader`
  String get ScreenReader {
    return Intl.message(
      'Screen Reader',
      name: 'ScreenReader',
      desc: '',
      args: [],
    );
  }

  /// `Go to settings`
  String get GoSettings {
    return Intl.message(
      'Go to settings',
      name: 'GoSettings',
      desc: '',
      args: [],
    );
  }

  /// `English`
  String get English {
    return Intl.message(
      'English',
      name: 'English',
      desc: '',
      args: [],
    );
  }

  /// `Spanish`
  String get Spanish {
    return Intl.message(
      'Spanish',
      name: 'Spanish',
      desc: '',
      args: [],
    );
  }

  /// `Language changed`
  String get LanguageChanged {
    return Intl.message(
      'Language changed',
      name: 'LanguageChanged',
      desc: '',
      args: [],
    );
  }

  /// `Options list, press for show options`
  String get ListLanguageOptions {
    return Intl.message(
      'Options list, press for show options',
      name: 'ListLanguageOptions',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en'),
      Locale.fromSubtags(languageCode: 'es'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    if (locale != null) {
      for (var supportedLocale in supportedLocales) {
        if (supportedLocale.languageCode == locale.languageCode) {
          return true;
        }
      }
    }
    return false;
  }
}