import 'package:accessibilityapp/application/font/textingBox.dart';
import 'package:accessibilityapp/application/notifier/languageNotifier.dart';
import 'package:accessibilityapp/generated/l10n.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'package:flutter_svg/flutter_svg.dart';

class Language extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _LanguageState();
}

class _LanguageState extends State<Language> {
  String dropdownValue = 'EN';

  @override
  Widget build(BuildContext context) {
    this.dropdownValue =
        Provider.of<LanguageNotifier>(context).setLocale(context);
    return Column(children: [
      _paddedLeftTop(5.0, 4.0, _labelSection()),
      _paddedLeft(25.0, _optionsSection())
    ]);
  }

  Row _labelSection() {
    return Row(
      children: <Widget>[_icon(), _box(), TextingBox(S.of(context).Language)],
    );
  }

  Padding _paddedLeft(double padding, Widget child) {
    return Padding(
      padding: EdgeInsets.only(left: padding),
      child: child,
    );
  }

  Padding _paddedLeftTop(double left, double top, Widget child) {
    return Padding(
      padding: EdgeInsets.only(left: left, top: top),
      child: child,
    );
  }

  Row _optionsSection() {
    return Row(
      children: [_box(), _selectLabel(), _box(), _wrapperSemanticsOption()],
    );
  }

  Widget _wrapperSemanticsOption() {
    return Semantics(
      excludeSemantics: true,
      explicitChildNodes: false,
      //value: "Español",
      label: S.of(context).ListLanguageOptions,
      child: _optionList(),
    );
  }

  SizedBox _box() {
    return SizedBox(
      width: 4.0,
    );
  }

  Icon _icon() {
    return Icon(Icons.language);
  }

  TextingBox _selectLabel() {
    return TextingBox(S.of(context).SelectLanguage);
  }

  Widget _optionList() {
    return DropdownButton(
        value: dropdownValue,
        icon: Icon(Icons.arrow_downward),
        iconSize: 14,
        elevation: 16,
        //style: TextStyle(color: Colors.deepPurple),
        style: _getTheme(),
        underline: Container(
          height: 2,
          color: Colors.blue[200],
        ),
        items: _listOption().map((OptionLanguage value) {
          return DropdownMenuItem<String>(
              value: value.option,
              child: Semantics(
                label: _codeToLanguage(value.option),
                excludeSemantics: true,
                child: Row(
                  children: <Widget>[
                    SvgPicture.asset(
                      value.icon,
                      height: 27.0,
                      width: 27.0,
                    ),
                    SizedBox(width: 4.0),
                    TextingBox(value.option)
                  ],
                ),
              ));
        }).toList(),
        onChanged: (String newValue) {
          setState(() {
            dropdownValue = newValue;
            Provider.of<LanguageNotifier>(context, listen: false)
                .changelocale(Locale(newValue.toLowerCase()));
            //show message notification
            Scaffold.of(context).showSnackBar(SnackBar(
                duration: Duration(seconds: 2),
                behavior: SnackBarBehavior.floating,
                content: TextingBox(S.of(context).LanguageChanged)));
          });
        });
  }

  List<OptionLanguage> _listOption() {
    List listLanguage = List<OptionLanguage>();
    OptionLanguage en = OptionLanguage("EN", "assets/img/fusa.svg");
    OptionLanguage es = OptionLanguage("ES", "assets/img/fspain.svg");
    listLanguage.add(en);
    listLanguage.add(es);
    return listLanguage;
  }

  TextStyle _getTheme() {
    if (Provider.of<LanguageNotifier>(context).mode == "Modo claro" ||
        Provider.of<LanguageNotifier>(context).mode == "Light mode")
      return TextStyle(color: Colors.white);
    else
      return TextStyle(color: Colors.black);
  }

  String _codeToLanguage(String code) {
    String current;
    switch (code) {
      case "ES":
        current = S.of(context).Spanish;
        return current;
      case "EN":
        return current = S.of(context).English;
      default:
        return "undefined";
    }
  }
}

class OptionLanguage {
  String option;
  String icon;
  OptionLanguage(this.option, this.icon);
  get getOption => option;
  get getIcon => icon;
}
