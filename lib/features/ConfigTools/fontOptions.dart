import 'package:accessibilityapp/application/font/textingBox.dart';
import 'package:accessibilityapp/application/notifier/fontsizeNotifier.dart';
import 'package:accessibilityapp/application/font/fontSize.dart';
import 'package:accessibilityapp/generated/l10n.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';

class OptionsFont extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _OptionFontState();
}

enum OptionSize { small, medium, large }

class _OptionFontState extends State {
  OptionSize _selected = OptionSize.medium; //change here for system preference

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        _paddedLeftTop(5.0, 4.0, _labelSection()),
        _paddedLeftTop(25.0, 8.0, TextingBox(S.of(context).selectFont)),
        _paddedLeft(25.0, _optionsList())
      ],
    );
  }

  Row _labelSection() {
    return Row(
      children: [
        Icon(Icons.format_size),
        _box(),
        TextingBox(S.of(context).FontSize)
      ],
    );
  }

  Padding _paddedLeft(double padding, Widget child) {
    return Padding(
      padding: EdgeInsets.only(left: padding),
      child: child,
    );
  }

  Padding _paddedLeftTop(double left, double top, Widget child) {
    return Padding(
      padding: EdgeInsets.only(left: left, top: top),
      child: child,
    );
  }

  SizedBox _box() {
    return SizedBox(
      width: 4.0,
    );
  }

  Column _optionsList() {
    return Column(
      children: <Widget>[
        rowOption(S.of(context).small, OptionSize.small),
        rowOption(S.of(context).medium, OptionSize.medium),
        rowOption(S.of(context).large, OptionSize.large),
      ],
    );
  }

  Row rowOption(String optionLabel, OptionSize size) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        optionButton(size),
        TextingBox(optionLabel),
      ],
    );
  }

  Radio optionButton(OptionSize size) {
    return Radio(
      value: size,
      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
      groupValue: _selected,
      onChanged: (value) {
        setState(() {
          _selected = value;
          debugPrint(size.toString());
          checkOption(size);
        });
      },
    );
  }

  void checkOption(OptionSize option) {
    if (option == OptionSize.small) {
      Provider.of<FontsizeNotifier>(context, listen: false)
          .fontSize(FontSize.small);
    } else if (option == OptionSize.medium) {
      Provider.of<FontsizeNotifier>(context, listen: false)
          .fontSize(FontSize.medium);
    } else if (option == OptionSize.large) {
      Provider.of<FontsizeNotifier>(context, listen: false)
          .fontSize(FontSize.large);
    }
  }
}
