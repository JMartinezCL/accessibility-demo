import 'package:accessibilityapp/application/font/textingBox.dart';
import 'package:accessibilityapp/generated/l10n.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:system_settings/system_settings.dart';

class ScreenReader extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _ScreenReaderState();
}

class _ScreenReaderState extends State {
  @override
  Widget build(BuildContext context) {
    return Column(
        crossAxisAlignment:
            CrossAxisAlignment.start, //if is not, it'll be center
        children: <Widget>[
          _paddedLeftTop(5.0, 4.0, _labelSection()),
          _paddedLeftTop(25.0, 8.0, _detectSR()),
          _paddedLeftTop(25.0, 8.0, _settings())
        ]);
  }

  Widget _settings() {
    return RaisedButton(
        onPressed: _goSettings, child: TextingBox(S.of(context).GoSettings));
  }

  void _goSettings() async {
    await SystemSettings.accessibility();
  }

  Row _labelSection() {
    return Row(
      children: <Widget>[
        Icon(Icons.speaker_notes),
        _box(),
        TextingBox(S.of(context).ScreenReader)
      ],
    );
  }

  Padding _paddedLeftTop(double left, double top, Widget child) {
    return Padding(
      padding: EdgeInsets.only(left: left, top: top),
      child: child,
    );
  }

  Padding _paddedLeft(double padding, Widget child) {
    return Padding(
      padding: EdgeInsets.only(left: padding),
      child: child,
    );
  }

  TextingBox _detectSR() {
    final mediaQueryData = MediaQuery.of(context);
    if (mediaQueryData.accessibleNavigation) {
      return TextingBox(S.of(context).SRStateOn);
    } else {
      return TextingBox(S.of(context).SRStateOff);
    }
  }

  SizedBox _box() {
    return SizedBox(
      width: 4.0,
    );
  }
}
