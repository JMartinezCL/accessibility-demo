import 'package:accessibilityapp/application/font/textingBox.dart';
import 'package:accessibilityapp/application/notifier/languageNotifier.dart';
import 'package:accessibilityapp/application/notifier/themeNotifier.dart';
import 'package:accessibilityapp/generated/l10n.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';

class ThemeOption extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _ThemeOptionsState();
}

class _ThemeOptionsState extends State {
  bool isSwitch = false;
  String mode = "Light";
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        _paddedLeftTop(5.0, 4.0, _labelSection()),
        _paddedLeft(25.0, switchOption())
      ],
    );
  }

  Row _labelSection() {
    return Row(
      children: [
        Icon(Icons.brightness_6),
        _box(),
        TextingBox(S.of(context).Theme)
      ],
    );
  }

  SizedBox _box() {
    return SizedBox(
      width: 4.0,
    );
  }

  Widget _switchButton() {
    if (Provider.of<LanguageNotifier>(context).mode == "Modo claro" ||
        Provider.of<LanguageNotifier>(context).mode == "Light mode")
      return TextingBox(S.of(context).darkMode);
    else
      return TextingBox(S.of(context).lightMode);
  }

  Widget _wrapperConsumer() {
    return Consumer<ThemeNotifier>(
      builder: (context, appState, child) {
        return optionTheme(appState, context);
      },
    );
  }

  Row switchOption() {
    return Row(
      children: [_switchButton(), _wrapperConsumer()],
    );
  }

  Padding _paddedLeft(double padding, Widget child) {
    return Padding(
      padding: EdgeInsets.only(left: padding),
      child: child,
    );
  }

  Padding _paddedLeftTop(double left, double top, Widget child) {
    return Padding(
      padding: EdgeInsets.only(left: left, top: top),
      child: child,
    );
  }

  Switch optionTheme(ThemeNotifier state, BuildContext context) {
    return Switch(
      value: isSwitch,
      onChanged: (value) {
        setState(() {
          this.isSwitch = value;
          state.switchTheme();
          Provider.of<LanguageNotifier>(context, listen: false)
              .changeThemeMode = value;
          this.mode = isSwitch ? "Light" : "Dark";
          Provider.of<LanguageNotifier>(context, listen: false)
              .changeThemeMode = value;
          Scaffold.of(context).showSnackBar(SnackBar(
              duration: Duration(seconds: 2),
              behavior: SnackBarBehavior.floating,
              content: Text('Theme changed')));
        });
      },
    );
  }
}
