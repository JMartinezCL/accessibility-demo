//import 'package:accessibilityapp/application/font/typographic.dart';
//import 'package:accessibilityapp/application/notifier/themeNotifier.dart';
import 'package:accessibilityapp/application/font/textingBox.dart';
import 'package:accessibilityapp/features/ConfigTools/ThemeOptions.dart';
import 'package:accessibilityapp/features/ConfigTools/fontOptions.dart';
import 'package:accessibilityapp/features/ConfigTools/screenReaderOption.dart';
import 'package:accessibilityapp/features/Languages/languageOptions.dart';
import 'package:accessibilityapp/generated/l10n.dart';
import 'package:flutter/material.dart';
//import 'package:provider/provider.dart';

class Home extends StatefulWidget {
  Home({Key key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

enum ChoiseSize { min, medium, max }

class _HomeState extends State<Home> {
  bool isSwitch = false;
  String mode = "Light";

  @override
  Widget build(BuildContext context) {
    //var textTheme = Theme.of(context).textTheme; //textTheme.option to use

    return Scaffold(
        appBar: AppBar(
          title: TextingBox(S.of(context).Menu),
        ),
        body: Center(
            child: Column(children: <Widget>[
          Language(),
          Divider(
            indent: 4.0,
            endIndent: 4.0,
          ),
          ThemeOption(),
          Divider(
            indent: 4.0,
            endIndent: 4.0,
          ),
          OptionsFont(),
          Divider(
            indent: 4.0,
            endIndent: 4.0,
          ),
          ScreenReader()
        ])));
  }

  TextingBox detectSR() {
    final mediaQueryData = MediaQuery.of(context);
    if (mediaQueryData.accessibleNavigation) {
      return TextingBox(S.of(context).SRStateOn);
    } else {
      return TextingBox(S.of(context).SRStateOff);
    }
  }

  Widget voice() {
    return Semantics(
      label: 'University of La Serena',
      hint: 'ScreenReader state',
      child: detectSR(),
    );
  }
}
