import 'package:accessibilityapp/application/notifier/languageNotifier.dart';
import 'package:accessibilityapp/application/routes/routesFactory.dart';
import 'package:accessibilityapp/application/routes/routesNames.dart';
import 'package:accessibilityapp/application/themes/themes.dart';
import 'package:accessibilityapp/generated/l10n.dart';
import 'package:flutter/material.dart';
import 'package:accessibilityapp/application/notifier/themeNotifier.dart';
import 'package:provider/provider.dart';

import 'notifier/fontsizeNotifier.dart';

import 'package:flutter_localizations/flutter_localizations.dart';

class MainApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => ThemeNotifier()),
        ChangeNotifierProvider(create: (context) => FontsizeNotifier()),
        ChangeNotifierProvider(create: (context) => LanguageNotifier())
      ],
      child: _wrapperConsumer(),
    );
    /*ChangeNotifierProvider(
      create: (context) => ThemeNotifier(),
      child: _wrapperConsumer(),
    );*/
  }

  Widget _wrapperConsumer() {
    return Consumer2<ThemeNotifier, LanguageNotifier>(
      builder: (context, appState, language, child) {
        return _buildApp(appState.isDarkMode, language.getLocale, context);
      },
    );
  }

  MaterialApp _buildApp(bool isDarkMode, Locale locale, context) {
    return MaterialApp(
      localizationsDelegates: [
        S.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: S.delegate.supportedLocales,
      locale: locale, //Provider.of<LanguageNotifier>(context).locale,
      title: "AccessTesting",
      debugShowCheckedModeBanner: false,
      theme: Themes.light,
      darkTheme: Themes.dark,
      themeMode: isDarkMode ? ThemeMode.dark : ThemeMode.light,
      initialRoute: RoutesName.initial,
      routes: RoutesFactory().credentialDefaults(),
    );
  }
}
