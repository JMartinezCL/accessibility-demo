import 'package:accessibilityapp/application/routes/routesNames.dart';
import 'package:accessibilityapp/features/home/home.dart';
import 'package:flutter/widgets.dart';

class RoutesFactory {
  Map<String, WidgetBuilder> credentialDefaults() {
    return {
      RoutesName.initial: (context) => Home(),
    };
  }
}
