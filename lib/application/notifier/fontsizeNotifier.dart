import 'package:flutter/widgets.dart';

class FontsizeNotifier extends ChangeNotifier {
  String size = "medium";
  double sizenumber = 14.0; //change for system config

  void fontSize(String currentSize) {
    this.size = currentSize;
    checkSize();
    notifyListeners();
  }

  void checkSize() {
    if (this.size == "small")
      sizenumber = 10.0;
    else if (this.size == "medium")
      sizenumber = 14.0;
    else if (this.size == "large") sizenumber = 18.0;
  }
}
