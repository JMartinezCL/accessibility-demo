import 'package:flutter/widgets.dart';

class ThemeNotifier extends ChangeNotifier {
  bool isDarkMode = false;

  void switchTheme() {
    this.isDarkMode = !isDarkMode;
    notifyListeners();
  }
}
