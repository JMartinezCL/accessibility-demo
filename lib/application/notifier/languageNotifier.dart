import 'package:accessibilityapp/generated/l10n.dart';
import 'package:flutter/widgets.dart';

class LanguageNotifier extends ChangeNotifier {
  Locale locale = Locale('es'); //change for system option
  String mode = "Light"; //change for system option
  Locale get getLocale => locale; //pasar context
  void changelocale(Locale l) {
    locale = l;
    notifyListeners();
  }

  set changeThemeMode(bool mode) {
    this.mode = mode ? S.current.lightMode : S.current.darkMode;
    debugPrint("Works :)${this.mode}");
    notifyListeners();
  }

  get getMode => mode;

  set setMode(String themeMode) {
    this.mode = themeMode;
    notifyListeners();
  }

  String setLocale(BuildContext context) {
    String country = Localizations.localeOf(context).languageCode;
    debugPrint(country);
    this.locale = Locale(country);
    return country.toUpperCase();
  }

  Locale getCurrentLocale(BuildContext context) {
    String code = Localizations.localeOf(context).languageCode;
    this.locale = Locale(code);
    return this.locale;
  }
}
