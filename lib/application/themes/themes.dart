import 'package:accessibilityapp/application/notifier/fontsizeNotifier.dart';
import 'package:flutter/material.dart';

class Themes {
  Themes._();

  static final ThemeData light = ThemeData.light().copyWith(
      //config to add
      textTheme: TextTheme(
              bodyText1: TextStyle(fontSize: FontsizeNotifier().sizenumber),
              bodyText2: TextStyle(fontSize: FontsizeNotifier().sizenumber),
              headline1: TextStyle(color: Colors.blueGrey),
              button: TextStyle(fontSize: FontsizeNotifier().sizenumber))
          .apply(bodyColor: Colors.black, displayColor: Colors.black),
      primaryColor: Colors.blue[900],
      dividerColor: Colors.grey[400],
      iconTheme: IconThemeData(color: Colors.blue[800]),
      buttonTheme: ButtonThemeData(
          buttonColor: Colors.blue[800], textTheme: ButtonTextTheme.primary));

  static final ThemeData dark = ThemeData.dark().copyWith(
      textTheme: TextTheme(
              bodyText1: TextStyle(fontSize: FontsizeNotifier().sizenumber),
              bodyText2: TextStyle(fontSize: FontsizeNotifier().sizenumber),
              headline1: TextStyle(color: Colors.blueGrey),
              button: TextStyle(fontSize: FontsizeNotifier().sizenumber))
          .apply(),
      dividerColor: Colors.blue[100],
      toggleableActiveColor: Colors.blue[50],
      iconTheme: IconThemeData(color: Colors.blue[50]),
      buttonTheme: ButtonThemeData(
          buttonColor: Colors.blue[50], textTheme: ButtonTextTheme.primary));
}
