import 'package:accessibilityapp/application/notifier/fontsizeNotifier.dart';
import 'package:accessibilityapp/application/notifier/languageNotifier.dart';
import 'package:accessibilityapp/generated/l10n.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';

class TextingBox extends StatelessWidget {
  final String text;
  TextingBox(this.text);
  @override
  Widget build(BuildContext context) {
    //double sizeFont = Provider.of<FontsizeNotifier>(context).sizenumber;
    return _wrapperConsumer(context);
  }

  Widget _wrapperConsumer(
    BuildContext context,
  ) {
    return Consumer<LanguageNotifier>(
      builder: (context, value, child) {
        double sizeFont = Provider.of<FontsizeNotifier>(context).sizenumber;
        return Text(text, style: TextStyle(fontSize: sizeFont));
      },
    );
  }
}
