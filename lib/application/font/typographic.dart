import 'package:flutter/material.dart';

class TypographicType {
  final TextStyle style;
  //String text;

  TypographicType(this.style);

  Text textBox(String text) {
    return Text(
      text,
      style: style,
    );
  }
}
