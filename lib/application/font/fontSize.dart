abstract class FontSize {
  static const String small = "small";
  static const String medium = "medium";
  static const String large = "large";
  //fontsize min
  static const double buttonMin = 12;
  static const double subtitleMin = 12;
  static const double headline1Min = 12;
  static const double headline2Min = 12;
  //fontsize max
  static const double buttonMax = 18;
  static const double subtitleMax = 18;
  static const double headline1Max = 18;
  static const double headline2Max = 18;
  //fontsize medium is for default
}
